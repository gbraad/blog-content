---
Title: Mastodon account
Date: 2022-12-16
Category: Social
Tags: mastodon, fediverse, social
---

I have moved to Mastodon at https://mastodon.social/@gbraad. I will be posting about things that interest me, which are the following, but not limited to.


My interests
  * #electronics
  * FPGA
  * micro:bit, RasPi, Arduino
  * RC models (#Tamiya, WL Toys)
  * #retro computers and consoles
  * #RetroGaming
  * 𝑨𝑴𝑰𝑮𝑨 / #Amiga
  * #Sega MegaDrive
  * #emulation, MiSTer
  * comparisons between platforms
  * #Linux and programming
  * #Fedora, #CentOS, #RHEL
  * Containers
  * #javascript, #python
  * Golang, #dotNet


Links
  * https://mastodon.social/@gbraad
  * https://pixelfed.social/gbraad
  * https://matrix.to/#/@gbraad:matrix.org
